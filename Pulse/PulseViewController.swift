//
//  ViewController.swift
//  Pulse
//
//  Created by michal on 15/08/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit

class PulseViewController: UIViewController {

    @IBOutlet weak var madafakaButton: UIButton!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var timeProgress: UIProgressView!
    
    var resultCounting: Int = 0
    var multiplicationResult: Int = 6
    
    var timeReadyLeft: Int = 3
    var timerStart: Timer!
    var timeLeft: Int = 10
    var timerPulse: Timer!
    let progress = Progress(totalUnitCount: 10)
    
    var color = Colors()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        
        color.setView(view: self.view)
        color.setButton(button: madafakaButton, view: self.view)
        madafakaButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        madafakaButton.addTarget(self, action: #selector(buttonReleased), for: .touchDown)
        
        timeProgress.isHidden = true
        
        progressLabel.text = "Get Ready"
        
        madafakaButton.layer.cornerRadius = madafakaButton.frame.height / 2
        madafakaButton.isEnabled = false
        timerStart = Timer.scheduledTimer(timeInterval: 0.9, target: self, selector: #selector(ready), userInfo: nil, repeats: true)
    }
    
    @IBAction func buttonPress(_ sender: Any) {
        resultCounting += 1
    }
    
    @objc func buttonReleased() {
        color.setButtonPressed(button: madafakaButton, view: self.view)
    }

    @objc func buttonPressed() {
        color.setButton(button: madafakaButton, view: self.view)
    }

    @objc func ready() {
        timeReadyLeft -= 1
        madafakaButton.setTitle(String(timeReadyLeft), for: .normal)
        
        if timeReadyLeft == 0 {
            timerStart.invalidate()
            
            timeProgress.isHidden = false
            madafakaButton.setTitle("Pulse", for: .normal)
            madafakaButton.isEnabled = true
            progressLabel.text = "Measuring"
            
            timerPulse = Timer.scheduledTimer(timeInterval: 1.00, target: self, selector: #selector(pulse), userInfo: nil, repeats: true)
        }
    }
    
    @objc func pulse() {
        timeLeft -= 1
        timeProgress.setProgress(Float(timeLeft)/10.00, animated: true)
        
        if timeLeft == 0 {
            timerPulse.invalidate()
            madafakaButton.isEnabled = false
            
            performSegue(withIdentifier: "pulse", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pulse" {
            let destinationResultVC = segue.destination as! ResultViewController
          
            destinationResultVC.result = resultCounting*multiplicationResult
        }
    }
    
    @IBAction func exitTapped(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
}
