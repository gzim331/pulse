//
//  StartViewController.swift
//  Pulse
//
//  Created by michal on 19/08/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var buttonStart: UIButton!
    
    var color = Colors()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonStart.layer.cornerRadius = buttonStart.frame.height / 2
        color.setButton(button: buttonStart, view: self.view)
        color.setView(view: self.view)
    }
    
    @IBAction func startCountPulse(_ sender: Any) {
        performSegue(withIdentifier: "pulseCounting", sender: self)
    }
    
}
