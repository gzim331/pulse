//
//  Pulse.swift
//  Pulse
//
//  Created by michal on 06/09/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

class Pulse {
    
    var id: String = ""
    var bpm: String = ""
    var date: String = ""
    
    func getData() -> String {
        return "\(bpm) BPM   [\(date)]"
    }
}
