//
//  ResultViewController.swift
//  Pulse
//
//  Created by michal on 19/08/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit
import Firebase

class ResultViewController: UIViewController {
    
    @IBOutlet weak var bpmLabel: UILabel!
    
    var result: Int = 0
    var color = Colors()
    
    let date = Date()
    let calendar = Calendar.current
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        
        color.setView(view: self.view)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeDown)
        
        showBpm()
    }
    
    func showBpm() {
        bpmLabel.text = "\(result) BPM"
    }

    @IBAction func exitTapped(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func swiped(_ gesture: UISwipeGestureRecognizer) {
        if gesture.direction == .right {
            navigationController?.popToRootViewController(animated: true)
        }
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func showDate() -> String {
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        return "\(hour):\(minutes) \(day).\(month).\(year)"
    }
    
    @IBAction func sendResult(_ sender: Any) {
        let historyDB = Database.database().reference().child("History")
        let key = historyDB.childByAutoId().key
        let historyDictionary = ["Bpm": String(result),
                                 "Date": showDate(),
                                 "id": key]
        historyDB.child(key!).setValue(historyDictionary) {
            (error, reference) in
            
            if error != nil {
                print(error!)
            } else {
                print("Pulse save successfully")
            }
        }
    }
    
    
}
