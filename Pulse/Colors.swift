//
//  Colors.swift
//  Pulse
//
//  Created by michal on 02/09/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit
import ChameleonFramework

class Colors {
    
    private let bgColor1 = UIColor(hexString: "281c49")! //250F3E-fiolet(ciemny)
    private let bgColor2 = UIColor(hexString: "72386b")! //8a387c
    private let bgColor3 = UIColor(hexString: "99383f")! //C42C3B-takiRybi
    
    private let colorButtonPressed1 = UIColor.flatBlueColorDark()!
    private let colorButtonPressed2 = UIColor.flatWhiteColorDark()!
    
    private let colorButton1 = UIColor.flatBlue()!
    private let colorButton2 = UIColor.flatWhite()!
    
    func setButton(button: UIButton, view: UIView) {
        button.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: view.frame, andColors: [colorButton1, colorButton2])
    }
    
    func setButtonPressed(button: UIButton, view: UIView) {
        button.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: view.frame, andColors: [colorButtonPressed1, colorButtonPressed2])
    }
    
    func setView(view: UIView) { // .topToBottom
        view.backgroundColor = UIColor.init(gradientStyle: .radial, withFrame: view.frame, andColors: [bgColor1, bgColor2, bgColor3])
    }
    
}
