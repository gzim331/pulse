//
//  HistoryViewController.swift
//  Pulse
//
//  Created by michal on 02/09/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit
import Firebase

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var historyTableView: UITableView!
    
    var color = Colors()
    
    var historyArray: [Pulse] = [Pulse]()
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference().child("History")
        
        retrieveHistory()
        
        color.setView(view: historyTableView)
        color.setView(view: self.view)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArray.count
    }
    
    //indexPath odnosi się do ścieżki indexu rowa
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyTableCell", for: indexPath)
        
        let row = indexPath.row
        cell.textLabel?.text = historyArray[row].bpm
        cell.detailTextLabel?.text = historyArray[row].date
        cell.backgroundColor = .clear
        return cell
    }
    
    func retrieveHistory() {
        let historyDB = Database.database().reference().child("History")
        historyDB.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as! Dictionary<String, String>
            
            let bpm = snapshotValue["Bpm"]!
            let date = snapshotValue["Date"]!
            let id = snapshotValue["id"]!
            
            let pulse = Pulse()
            pulse.bpm = bpm
            pulse.date = date
            pulse.id = id
            
            self.historyArray.append(pulse)
            self.historyTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            
            let pulse = historyArray[indexPath.row]
            ref.child(pulse.id).setValue(nil)
            
            self.historyArray.remove(at: indexPath.row)
            self.historyTableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.left)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        historyTableView.deselectRow(at: indexPath, animated: true)
    }
}
