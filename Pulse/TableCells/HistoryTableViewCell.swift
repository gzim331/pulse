//
//  HistoryTableViewCell.swift
//  Pulse
//
//  Created by michal on 06/09/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var bpmLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
