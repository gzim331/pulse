//
//  BpmDelegate.swift
//  Pulse
//
//  Created by michal on 11/09/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

protocol BpmDelegate {
    func getBpm(pulse: Pulse)
}
